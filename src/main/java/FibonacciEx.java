/**
 * Created by dogoran on 01.04.2017.
 */
public final class FibonacciEx {

    public static int[] getFibonacciSequance(int num) {

        int[] arr = new int[num];

        int first = 0;
        int second = 1;
        int last;

        arr[0] = first;
        arr[1] = second;
        for(int i = 2; i < num; i++) {
            last = first + second;
            first = second;
            second = last;
            arr[i] = last;
        }

        return arr;
    }

}
