/**
 * Created by dogoran on 01.04.2017.
 */
public class Address {

    private String street;
    private int houseId;

    public Address(String street, int houseId){

        this.street = street;
        this.houseId = houseId;

    }


    public Address(int houseId){

        this.houseId = houseId;

    }


    public Address(String street){

        this.street = street;

    }

    public void setHouseId(int houseId){

        this.houseId = houseId;

    }

    public int getHouseId(){
        return this.houseId;
    }

    public void setStreet(String street){

        this.street = street;

    }

    public String getStreet(){

        return this.street;

    }

}
