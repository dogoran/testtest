import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

/**
 * Created by dogoran on 01.04.2017.
 */
public class Main {
    public static void main(String[] args) {

        ArrayList<Human> humanList = new ArrayList<Human>();

        humanList.add(0, new Human("Mad","Lol",
                new Date(2007, 1, 12),
                new Address("Washington", 6)));
        humanList.add(1, new Human("Lich","Knight",
                new Date(1998, 8, 11),
                new Address("Solar", 200)));
        humanList.add(2, new Human("Jordon","Sparks",
                new Date(1977, 2, 14),
                new Address("Row", 12)));
        humanList.add(3, new Human("Lamer","Best",
                new Date(2017, 3, 29),
                new Address("Cartman", 102)));
        humanList.add(4, new Human("John","Lourence",
                new Date(1980, 2, 16),
                new Address("Cartman", 122)));

        Collections.sort(humanList);

        // 1
        System.out.println("By Last Name");
        searchByLastName(humanList, "Best");

        // 2
        System.out.println("By Address");
        searchByAddressAttrib(humanList, new Address("Cartman"));

        // 3
        System.out.println("In Date Range");
        searchInDateRange(humanList, new Date(1990,2,16),
                new Date(2010, 1,1));

        // 4
        System.out.println("By Age");
        System.out.println(searchByAge(humanList, "OLDEST").toString());

        // 5
        System.out.println("Ppl on the Same Street:");
        System.out.println( " ");
        searchPeopleOnTheSameStreet(humanList, new Address("Cartman"));

        // Fibonacci
        System.out.println("Fibonacci sequance:");
        int[] fibonacci = FibonacciEx.getFibonacciSequance(15);
        for(int n : fibonacci) {
            System.out.print(n + " ");
        }
    }


    private static boolean searchByLastName(ArrayList<Human> list, String lastName) {
        for(Human h : list) {
            if((h.lastName).equals(lastName)) {
                System.out.print(h.toString() + "\n");
                return true;
            }
        }
        return false;
    }
    private static Human searchByAge(ArrayList<Human> list, String flag) {
        if(flag.equals("OLDEST")) {
            return list.get(0);
        }
        if(flag.equals("YOUNGEST")) {
            return list.get(list.size() - 1);
        }
        return new Human();
    }
    private static void searchPeopleOnTheSameStreet(ArrayList<Human> list, Address address) {
        for(Human h : list) {
            if(h.getAddress().getStreet().equals(address.getStreet())) {
                System.out.println(h.toString());
            }
        }
    }
    private static boolean searchByAddressAttrib(ArrayList<Human> list, Address address) {
        if(address.getStreet() != null) {
            for(Human h : list) {
                if(h.getAddress().getStreet().equals(address.getStreet())) {
                    System.out.println(h.toString());
                    return true;
                }
            }
        }
        else {
            for(Human h : list) {
                if(h.getAddress().getStreet().equals(address.getHouseId())) {
                    System.out.println(h.toString());
                    return true;
                }
            }
        }
        return false;
    }
    private static void searchInDateRange(ArrayList<Human> list, Date leftRange, Date rightRange) {
        for(Human h : list) {
            if(h.getBirthDate().getTime() > leftRange.getTime() &&
                    h.getBirthDate().getTime() < rightRange.getTime()) {
                System.out.println(h.toString());
            }
        }
    }
}
