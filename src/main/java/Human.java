import java.util.Date;

/**
 * Created by dogoran on 01.04.2017.
 */
public class Human  implements Comparable<Human>{

    public String firstName;
    public String lastName;
    public Date birthDate;
    private Address address;

    public Human(){}

    public Human(String firstName, String lastName, Date birthDate, Address address){

        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.address = address;

    }

    public void setFirstName(String firstName){

        this.firstName = firstName;

    }

    public void setLastName(String lastName){

        this.lastName = lastName;

    }

    public void setBirthDate(Date birthDate){

        this.birthDate = birthDate;

    }

    public void setAddress(Address address){

        this.address = address;

    }

    public String getFirstName(){

        return this.firstName;

    }

    public String getLastName(){

        return this.lastName;

    }

    public Date getBirthDate(){

        return this.birthDate;

    }

    public Address getAddress(){

        return this.address;

    }

    @Override
    public int compareTo(Human obj){
        return this.getBirthDate().compareTo(obj.getBirthDate());
    }

    @Override
    public String toString() {
        return "First Name: " + getFirstName() + " Last Name: " + getLastName() +
                " BirthDate: " + getBirthDate().getTime() + " Address:Street: " + getAddress().getStreet() +
                " Address:HouseId: " + getAddress().getHouseId();
    }
    @Override
    public boolean equals(Object obj) {
        Human tmp = (Human)obj;

        if(this.firstName == tmp.firstName) {
            return true;
        }
        return false;
    }

}
